import requests

def get_m3u_file(m3u_source):
    r = requests.get(m3u_source, allow_redirects=True)
    rr = r.text.split('\n')
    return rr

def common_list_creator():
    content = get_m3u_file('https://smarttvnews.ru/apps/iptvchannels.m3u')
    
    prg_list = []
    url_list = []
    common_list = []

    for i in content:
        if "EXTINF" in i:
            prg_list.append( i.split(',')[1])
        if 'http' in i:
            url_list.append(i)


    for i in range (0, len(prg_list)-1):
        common_list.append([prg_list[i], url_list[i]])

    return common_list