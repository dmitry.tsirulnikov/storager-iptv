from flask import Blueprint, render_template, request

main = Blueprint('main', __name__)

@main.route('/')
def iptv():
    from .parser import common_list_creator
    prg_list = common_list_creator()
    return render_template('iptv.html', prg_list = prg_list)