#Grab the latest alpine image
FROM alpine:latest

# Install python and pip
RUN apk add --no-cache --update python3 py3-pip bash
ADD ./requirements.txt /tmp/requirements.txt


# Install dependencies
RUN pip3 install --no-cache-dir -q -r /tmp/requirements.txt
RUN adduser -D myuser
# Add our code
COPY --chown=myuser:myuser ./ /app

ENV FLASK_APP=/app

WORKDIR /app

# Expose is NOT supported by Heroku
# EXPOSE 5000 		

# Run the image as a non-root user
USER myuser

CMD ["/bin/ash", "-c", "/usr/bin/flask run -h 0.0.0.0 -p $PORT"]
